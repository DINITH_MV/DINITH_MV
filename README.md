<body>   
<div align="center" width="100%">
    <img src="https://readme-typing-svg.herokuapp.com/?font=Amita&color=bb9071&size=35&center=true&vCenter=true&width=500&height=70&duration=4000&lines=Hi,+Hey+There!;I'm+Dinith+Vanderlan!;Welcome+to+my+Gitlab+Profile!;" />
</div>
    
- 🔭 I’m currently working on <strong>Angular projects</strong><br/>
- 👯 I’m looking to collaborate on <strong>Next JS projects</strong><br/>
- 💬 Ask me about <strong>React,Tailwind CSS</strong><br/>
<br/>

<div>
<h2 align="center" >⚡Languages and Tools</h2>
<p align="center">
 <img src="https://skillicons.dev/icons?i=js,react,nodejs,mongodb,html,css,postman,tailwind,figma,c,cpp,angular,vscode&theme=light" />
<br/>
<img src="https://skillicons.dev/icons?i=gitlab,ps,java,python,ts,php,mysql,firebase,kotlin,arduino,git,visualstudio,androidstudio&theme=light" />
</p><br/>

</div>
</body>